package com.wincentzzz.project.template.springhack.services;

import com.wincentzzz.project.template.springhack.dto.response.UserResponse;

public interface UserService {

    UserResponse login();
}
